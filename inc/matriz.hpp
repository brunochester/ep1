#ifndef MATRIZ_HPP
#define MATRIZ_HPP

using namespace std;

class Matriz{
   private:
     int feito;
     int tamanho;
     char mapa[30][30];
   
   public:
     Matriz();
     ~Matriz();

     void setFeito(int feito);
     int getFeito();

     void setTamanho(int tamanho);
     int getTamanho();

     void setMapa(char celula, int linha, int coluna);
     char getMapa(int linha, int coluna);
};

#endif
