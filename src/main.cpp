#include <iostream>
#include "matriz.hpp"
#include "gosperglidergun.hpp"

using namespace std;

int celulasvivas(Gosperglidergun save, int linha, int coluna){
   int vivos = 0;
       if(save.getMapa(linha-1,coluna) == '*'){
            vivos++;
       }
       if(save.getMapa(linha-1,coluna-1) == '*'){
            vivos++;
       }
       if(save.getMapa(linha,coluna-1) == '*'){
            vivos++;
       }
       if(save.getMapa(linha-1,coluna+1) == '*'){
            vivos++;
       }
       if(save.getMapa(linha+1,coluna-1) == '*'){
            vivos++;
       }
       if(save.getMapa(linha,coluna+1) == '*'){
            vivos++;
       }
       if(save.getMapa(linha+1,coluna+1) == '*'){
            vivos++;
       }
       if(save.getMapa(linha+1,coluna) == '*'){
            vivos++;
       }
    return vivos;
}

int main (int argc, char ** argv){
   int i,j;
   int celulas;
   int feito = 0;
   Gosperglidergun gospinho;
   Gosperglidergun memoria1;

   
   while(feito < gospinho.getFeito()){
      feito++;
      for(i=0;i<30;i++){
     	for(j=0;j<30;j++){
	  cout << gospinho.getMapa(i,j) << " ";
           if(i!=29 && i!=0 && j!=29 && j!=0){
		celulas = celulasvivas(memoria1,i,j);
		if(gospinho.getMapa(i,j) == '*' && (celulas < 2 || celulas > 3)){
		  gospinho.setMapa('*',i,j);
                } 
		if(gospinho.getMapa(i,j) == '*' && celulas == 3){
                  gospinho.setMapa('*',i,j);
                }
                if(gospinho.getMapa(i,j) == '*' && (celulas == 2 || celulas == 3)){
                  gospinho.setMapa('*',i,j);
                }
            }
         }
       }
       cout << endl;
   } 
   memoria1 = gospinho;
   cout << "\n";

return 0;
}   
