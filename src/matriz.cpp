#include <iostream>
#include "matriz.hpp"

Matriz::Matriz(){
   int i,j;
   tamanho = 0;
   for(i=0;i<30;i++){
      for(j=0;j<30;j++){
         mapa[i][j] = '-';
      }
    }
    feito = 0;
}

Matriz::~Matriz(){
}

int Matriz::getFeito(){
   return feito;
}
void Matriz::setFeito(int feito){
   this->feito=feito;
}
int Matriz::getTamanho(){
   return tamanho;
}
void Matriz::setTamanho(int tamanho){
   this->tamanho=tamanho;
}
char Matriz::getMapa(int linha, int coluna){
   return mapa[linha][coluna];
}
void Matriz::setMapa(char celula, int linha, int coluna){
   mapa[linha][coluna]=celula;
}
